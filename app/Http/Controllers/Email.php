<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use PHPMailer\PHPMailer\PHPMailer;

class Email extends Controller
{
    public function contact(Request $request){
        $subject = "Asunto del correo";
        $for = "lfernando@integrador-technology.mx";
        Mail::send('email',$request->all(), function($msj) use($subject,$for){
            $msj->from("ocefacturacion@gmail.com","Papu Funciona");
            $msj->subject($subject);
            $msj->to($for);
        });

        $mail = new PHPMailer;
//Tell PHPMailer to use SMTP
        $mail->isSMTP();
//Enable SMTP debugging
// 0 = off (for production use)
// 1 = client messages
// 2 = client and server messages
        $mail->SMTPDebug = 2;
//Ask for HTML-friendly debug output
        $mail->Debugoutput = 'html';
//Set the hostname of the mail server
        $mail->Host = 'smtp.gmail.com';
// use
// $mail->Host = gethostbyname('smtp.gmail.com');
// if your network does not support SMTP over IPv6
//Set the SMTP port number - 587 for authenticated TLS, a.k.a. RFC4409 SMTP submission
        $mail->Port = 587;
//Set the encryption system to use - ssl (deprecated) or tls
        $mail->SMTPSecure = 'tls';
//Whether to use SMTP authentication
        $mail->SMTPAuth = true;
//Username to use for SMTP authentication - use full email address for gmail
        $mail->Username = "ocefacturacion@gmail.com";
//Password to use for SMTP authentication
        $mail->Password = "Integrador1";
//Set who the message is to be sent from
        $mail->setFrom('ocefacturacion@gmail.com', 'Facturacion Central');
//Set an alternative reply-to address
        //$mail->addReplyTo('replyto@example.com', 'First Last');
//Set who the message is to be sent to
        $mail->addAddress('lfernando@integrador-technology.mx', 'John Wick');
//Set the subject line
        $mail->Subject = 'PHPMailer GMail SMTP test';
//Read an HTML message body from an external file, convert referenced images to embedded,
//convert HTML into a basic plain-text alternative body
         $mail->msgHTML('Hola mundo',dirname(__FILE__));
//Replace the plain text body with one created manually
        $mail->AltBody = 'This is a plain-text message body';
//Attach an image file
        //$mail->addAttachment('images/phpmailer_mini.png');
//send the message, check for errors
        if (!$mail->send()) {
            dd( "Mailer Error: " . $mail->ErrorInfo);
        }

        return redirect()->back();
    }
}
